package org.example;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void calcularExpressao() {
        assertEquals(3,Main.calcularExpressao("1+2"));
        assertEquals(0,Main.calcularExpressao("1-1"));
        assertEquals(15,Main.calcularExpressao("5*3"));
        assertEquals(2.5,Main.calcularExpressao("10/4"));


    }
}