package org.example;

import java.util.Scanner;

public class Main {


    public static double calcularExpressao(String expressao) {
        String[] partes = expressao.split("\\s*[-+*/]\\s*");

        if (partes.length != 2) {
            throw new IllegalArgumentException("A expressão deve conter um único operador.");
        }

        double num1 = Double.parseDouble(partes[0]);
        double num2 = Double.parseDouble(partes[1]);
        char operador = expressao.replaceAll(".*([-+*/]).*", "$1").charAt(0);

        switch (operador) {
            case '+':
                return num1 + num2;
            case '-':
                return num1 - num2;
            case '*':
                return num1 * num2;
            case '/':
                if (num2 == 0) {
                    throw new ArithmeticException("Divisão por zero não é permitida.");
                }
                return num1 / num2;
            default:
                throw new IllegalArgumentException("Operador inválido: " + operador);
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Calculadora Simples");
            System.out.println("Digite uma expressão matemática com um único operador (+, -, *, /) ou 'sair' para encerrar:");

            String entrada = scanner.nextLine();

            if (entrada.equalsIgnoreCase("sair")) {
                System.out.println("A calculadora foi encerrada.");
                break;
            }

            try {
                double resultado = calcularExpressao(entrada);
                System.out.println("Resultado: " + resultado);
            } catch (IllegalArgumentException e) {
                System.out.println("Erro: Expressão inválida. Certifique-se de usar um único operador.");
            } catch (ArithmeticException e) {
                System.out.println("Erro: Divisão por zero não é permitida.");
            }
        }

        scanner.close();
    }

}